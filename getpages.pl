#!/usr/bin/perl

use strict;
use Mojo::DOM;
use Mojo::UserAgent;
use Mojo::Util qw(dumper spurt);
use Mojo::UserAgent::CookieJar;
use feature 'say';

my $ua = Mojo::UserAgent->new;
$ua = $ua->cookie_jar( Mojo::UserAgent::CookieJar->new );

$ua->on(start => sub {
  my ($ua, $tx) = @_;
  $tx->req->headers->header('Referer' => 'https://www.wlw.de/');
  $tx->req->headers->header('User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36');
});


my %links = (
# 1
#'https://www.wlw.de/treffer/ausstechmaschinen-fuer-baeckereien.html' => undef,
#'https://www.wlw.de/treffer/backanlagen-fuer-baeckereien.html' => undef,
#'https://www.wlw.de/treffer/baeckereiarbeitstische.html' => undef,
#'https://www.wlw.de/treffer/baeckereibedarf.html' => undef,
#'https://www.wlw.de/treffer/baeckereigeraete-aus-holz.html' => undef,
# 2
#'https://www.wlw.de/treffer/baeckereimaschinen.html' => undef,
#'https://www.wlw.de/treffer/gebrauchte-baeckereimaschinen.html' => undef,
#'https://www.wlw.de/treffer/baeckereiplanung.html' => undef,
#'https://www.wlw.de/treffer/baeckereitransportgeraete.html' => undef,
'https://www.wlw.de/treffer/baeckereiwaagen.html' => undef,
# 3
#'https://www.wlw.de/treffer/buersten-fuer-baeckereien.html' => undef,
'https://www.wlw.de/treffer/dekore-fuer-baeckereien-und-konditoreien.html' => undef,
#'https://www.wlw.de/treffer/dressier-und-spritzmaschinen-fuer-baeckereien.html' => undef,
'https://www.wlw.de/treffer/foerderbaender-fuer-baeckereien-und-konditoreien.html' => undef,
#'https://www.wlw.de/treffer/formwalzen-fuer-baeckereien.html' => undef,
# 4
#'https://www.wlw.de/treffer/gaerguttraeger-fuer-baeckereien.html' => undef,
#'https://www.wlw.de/treffer/gaerschraenke-fuer-baeckereien.html' => undef,
#'https://www.wlw.de/treffer/kaelteanlagen-fuer-baeckereien.html' => undef,
#'https://www.wlw.de/treffer/ladeneinrichtungen-fuer-baeckereien.html' => undef,
#'https://www.wlw.de/treffer/mehlsilos-fuer-baeckereien.html' => undef,
# 5
#'https://www.wlw.de/treffer/reinigung-von-baeckereitransportgeraeten.html' => undef,
#'https://www.wlw.de/treffer/reparaturen-von-baeckereimaschinen-und-baeckereianlagen.html' => undef,
#'https://www.wlw.de/treffer/software-fuer-baeckereien-und-konditoreien.html' => undef,
#'https://www.wlw.de/treffer/sonderanlagen-fuer-die-baeckereiindustrie.html' => undef,
# 6
#'https://www.wlw.de/treffer/staubsauger-fuer-baeckereien.html' => undef,
#'https://www.wlw.de/treffer/steuerungen-fuer-baeckereioefen.html' => undef,
#'https://www.wlw.de/treffer/textilien-fuer-baeckereien.html' => undef,
#'https://www.wlw.de/treffer/waschanlagen-fuer-baeckereien.html' => undef,
# 7
#'https://www.wlw.de/treffer/werbemittel-fuer-baeckereien.html' => undef,
#'https://www.wlw.de/treffer/baecker-und-kochbekleidung.html' => undef,
#'https://www.wlw.de/treffer/baeckerbeutel.html' => undef,
#'https://www.wlw.de/treffer/baeckerfroster.html' => undef,
# 8
#'https://www.wlw.de/treffer/baeckerhandschuhe.html' => undef,
#'https://www.wlw.de/treffer/baeckerzangen.html' => undef,
#'https://www.wlw.de/treffer/baeckereigeraete-aus-holz.html' => undef,
'https://www.wlw.de/treffer/foerderbaender-fuer-baeckereien-und-konditoreien.html' => undef,
# 9
#'https://www.wlw.de/treffer/formwalzen-fuer-baeckereien.html' => undef,
#'https://www.wlw.de/treffer/mehlsilos-fuer-baeckereien.html' => undef,
);

`mkdir -p data`;

my $skip = 0;

foreach my $link (keys %links) {
	say "$link";
	
	if ( $link eq 'https://www.wlw.de/treffer/foerderbaender-fuer-baeckereien-und-konditoreien.html') {
		$skip = 1;
	}
	
	Mojo::DOM->new( $ua->get($link)->res->body )->find('div.vcard a')->each(sub {
		my $a = shift;

		if ($a->{href} eq 'https://www.wlw.de/p/karl-kuhne-gmbh-co-kg-283538?heading_id=83021') {
			$skip = 0;
		}

		unless ($skip) {

			my ( $name,$num ) = ( $a->{href} =~ /\/p\/(.+)\?heading_id=(\d+)$/ );
		
			if ( $name and $num ) {
				say "\t$a->{href}";
	#			say $a->{href}."\n\t$name\n\t$num";
				$links{$link} = $num;
			
				$ua->get( $a->{href} )->res->content->asset->move_to("data/$num-$name.html");
			};
		}
	});
}

spurt dumper(%links) , 'link-dump.txt';

