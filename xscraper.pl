#!/usr/bin/perl

use strict;
use Mojo::DOM;
use Mojo::Util qw(slurp trim);
use Encode;
use feature ('say');

use Excel::Writer::XLSX;

my @data = (
{
	link => 'https://www.wlw.de/treffer/ausstechmaschinen-fuer-baeckereien.html',
	num => 61548,
	docs => [
		'anlagenkonstruktion-nord-gmbh-1674374'
		,'alfred-pfersich-gmbh-co-kg-388873'
		,'rego-herlitzius-gmbh-1585914'
		,'altuntas-gmbh-1677164'
		,'fischer-maschinenhandel-ohg-1530262'
		,'baeckereitechnik-benath-1250948'
	]
}
,{
	link => 'https://www.wlw.de/treffer/backanlagen-fuer-baeckereien.html',
	num => 61549,
	docs => [
		'otto-fischer-774922'
		,'tarte-gourmet-flammkuchen-service-gmbh-1130319'
		,'unifiller-europe-gmbh-1687154'
		,'anlagenkonstruktion-nord-gmbh-1674374'
	]
}
,{
	link => 'https://www.wlw.de/treffer/baeckereiarbeitstische.html',
	num => 61626,
	docs => [
		'bmtec-gmbh-585959'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'ferroconcept-ek-1722439'
		,'kunststoff-glogner-gmbh-co-kg-910769'
		,'anneliese-backtechnik-gmbh-1752625'
		,'gewerbemoebelde-1484888'
	]
}
,{
	link => 'https://www.wlw.de/treffer/baeckereibedarf.html',
	num => 123183,
	docs => [
		'agl-ausstattungsservice-fuer-gastronomie-und-lebensmittelhandwerk-gmbh-584387'
		,'edna-international-gmbh-338915'
		,'emil-schmidt-gmbh-1105573'
		,'georg-fles-gmbh-1519153'
		,'kelux-kunststoffe-gmbh-1396053'
		,'otto-fischer-774922'
		,'abendschoen-maschinenbau-gmbh-606188'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'boyens-backservice-gmbh-275815'
		,'kdputz-ek-283164'
		,'sam-baeckereitechnik-gmbh-537150'
		,'feuma-gastromaschinen-gmbh-460446'
		,'aws-automation-woelbern-sauermann-gmbh-co-kg-957207'
		,'bechtolina-fabrik-jean-bechtold-gmbh-co-kg-310967'
		,'biothek-gmbh-1602509'
		,'bmtec-gmbh-585959'
		,'gastro-mercado-sl-1321710'
		,'guenthart-co-kg-352733'
		,'karodur-pressplatten-gmbh-612327'
		,'kurfer-holzverarbeitung-1267468'
		,'limex-lebensmittelhandelsges-mbh-1048796'
		,'mohn-gmbh-1026044'
		,'muehlenbau-und-anlagenmontagen-gmbh-soeren-kolenda-389322'
		,'niederrheinische-formenfabrik-gerh-janssen-sohn-gmbh-co-kg-335129'
		,'tarte-gourmet-flammkuchen-service-gmbh-1130319'
	]
}
,{
	link => 'https://www.wlw.de/treffer/baeckereigeraete-aus-holz.html',
	num => 61628,
	docs => [
		'kurfer-holzverarbeitung-1267468'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'backshop24-inh-hilke-schebitz-1696549'
		,'dreier-gmbh-1270993'
		,'e-may-co-gmbh-603473'
		,'gebrueder-uibrig-gmbh-1114551'
		,'hermann-dertenkoetter-975904'
	]
},{
	link => 'https://www.wlw.de/treffer/baeckereimaschinen.html',
	num => 134844,
	docs => [
		'emil-schmidt-gmbh-1105573'
		,'moewius-gmbh-531703'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'franz-mensch-gmbh-337310'
		,'cerva-arbeitskleidung-gmbh-1582158'
		,'hjb-beratung-gmbh-1620808'
		,'anita-zeiler-arbeitsschutz-u-industriebedarf-1467754'
	]
}
#,{
#	link => 'https://www.wlw.de/treffer/baeckerhandschuhe.html',
#	num => 134844,
#	docs => [
#		'emil-schmidt-gmbh-1105573'
#		,'moewius-gmbh-531703'
#		,'andreas-kopp-inh-reiner-schebitz-1696552'
#		,'franz-mensch-gmbh-337310'
#		,'cerva-arbeitskleidung-gmbh-1582158'
#		,'hjb-beratung-gmbh-1620808'
#		,'anita-zeiler-arbeitsschutz-u-industriebedarf-1467754'
#}
,{
	link => 'https://www.wlw.de/treffer/gebrauchte-baeckereimaschinen.html',
	num => 61624,
	docs => [
		'otto-fischer-774922'
		,'sam-baeckereitechnik-gmbh-537150'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'kdputz-ek-283164'
		,'baktec-gmbh-1352111'
		,'a-bis-z-handelsagentur-kefenbaum-e-k-1684936'
		,'akbulut-gbr-1566941'
	]
},{
	link => 'https://www.wlw.de/treffer/baeckereiplanung.html',
	num => 148837,
	docs => [
		'andreas-kopp-inh-reiner-schebitz-1696552'
		,'otto-fischer-774922'
		,'bewo-gmbh-1104992'
		,'modemann-gmbh-1601772'
		,'zmk-gruppe-gmbh-1565286'
		,'lassen-und-lassen-innenausbau-gmbh-1313407'
		,'backtechnik-si-gmbh-593613'
	]
},{
	link => 'https://www.wlw.de/treffer/baeckereitransportgeraete.html',
	num => 61641,
	docs => [
		'andreas-kopp-inh-reiner-schebitz-1696552'
		,'ant-transportgeraete-1386176'
		,'kreckler-gmbh-663053'
		,'anneliese-backtechnik-gmbh-1752625'
		,'backshop24-inh-hilke-schebitz-1696549'
		,'latuske-inh-martin-rahm-1674224'
		,'dreier-gmbh-1270993'
	]
},{
	link => 'https://www.wlw.de/treffer/baeckereiwaagen.html',
	num => 92288,
	docs => [
		'andreas-kopp-inh-reiner-schebitz-1696552'
		,'erno-waegetechnik-gmbh-397751'
		,'h-visser-waegetechnik-gmbh-942034'
	]
},{
	link => 'https://www.wlw.de/treffer/buersten-fuer-baeckereien.html',
	num => 61627,
	docs => [
		'andreas-kopp-inh-reiner-schebitz-1696552'
		,'august-mink-kg-288449'
		,'eurobuersten-ug-969943'
		,'ibb-industriebuersten-gmbh-568898'
		,'niebling-technische-buersten-gmbh-302804'
		,'nordische-buerstenfertigung-gmbh-307929'
		,'osborn-international-gmbh-315955'
		,'pse-werkzeuge-und-maschinen-gmbh-427233'
		,'saettele-gmbh-co-kg-287680'
	]
},{
	link => 'https://www.wlw.de/treffer/dekore-fuer-baeckereien-und-konditoreien.html',
	num => 74947,
	docs => [
		'sybille-zanoll-1435185'
		,'guenthart-co-kg-352733'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'bechtolina-fabrik-jean-bechtold-gmbh-co-kg-310967'
		,'edna-international-gmbh-338915'
		,'amanda-nussverarbeitungsbetrieb-inh-e-blok-ek-491372'
	]
},{
	link => 'https://www.wlw.de/treffer/dressier-und-spritzmaschinen-fuer-baeckereien.html',
	num => 61582,
	docs => [
		'niederrheinische-formenfabrik-gerh-janssen-sohn-gmbh-co-kg-335129'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'krumbein-rationell-989731'
		,'tecon-keks-und-waffelanlagen-gmbh-co-kg-548117'
	]
},{
	link => 'https://www.wlw.de/treffer/foerderbaender-fuer-baeckereien-und-konditoreien.html',
	num => 83021,
	docs => [
		'ammeraal-beltech-gmbh-377072'
		,'itt-international-technical-textiles-gmbh-1741002'
		,'schloesser-van-dinther-gmbh-spezialbandfabrik-321778'
		,'bandzentrale-ingenieurgesellschaft-fuer-antriebs-und-transportbandtechnik-mbh-543853'
		,'j-brosius-gmbh-909832'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'bmtec-gmbh-585959'
		,'boston-gmbh-833998'
		,'dr-ing-bender-wippern-handels-gmbh-456289'
		,'ebm-bergmeier-gmbh-co-kg-364172'
		,'emil-gewehr-gmbh-290152'
		,'ffv-flensburger-foerderanlagen-und-vorrichtungsbau-junge-gmbh-610870'
		,'foerderbaender-stoll-gmbh-595990'
		,'foerdertechnik-ulf-kecke-gmbh-608046'
		,'hadi-gmbh-industrievertretungen-596176'
		,'hartmut-ulbrich-e-k-technische-gummiwaren-361917'
		,'karl-kuhne-gmbh-co-kg-283538'
		,'keiper-gmbh-co-kg-antriebs-u-transporttechnik-342421'
		,'kmt-produktions-montagetechnik-gmbh-273465'
		,'livio-bartel-gmbh-985216'
		,'manfred-kunze-gmbh-industrievulkanisation-372107'
		,'maschinenbau-kitz-gmbh-458284'
		,'schulz-drahttechnik-gmbh-531870'
	]
}
,{
	link => 'https://www.wlw.de/treffer/formwalzen-fuer-baeckereien.html',
	num => 137662,
	docs => [
		'abend-maschinenbau-ek-472240'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'egberts-rubber-b-v-1712965'
		,'internorm-kunststofftechnik-gmbh-541423'
		,'niederrheinische-formenfabrik-gerh-janssen-sohn-gmbh-co-kg-335129'
		,'edwin-blonske-1260093'
		,'rwstechnology-ltd-1734628'
	]
},{
	link => 'https://www.wlw.de/treffer/gaerguttraeger-fuer-baeckereien.html',
	num => 159308,
	docs => [
		'itt-international-technical-textiles-gmbh-1741002'
		,'ringoplast-gmbh-487640'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'niederrheinische-formenfabrik-gerh-janssen-sohn-gmbh-co-kg-335129'
		,'itt-international-technical-textiles-gmbh-1688089'
		,'dreier-gmbh-1270993'
		,'rennhak-baeckerei-technik-gmbh-1366093'
	]
},{
	link => 'https://www.wlw.de/treffer/gaerschraenke-fuer-baeckereien.html',
	num => 61552,
	docs => [
		'andreas-kopp-inh-reiner-schebitz-1696552'
		,'bmtec-gmbh-585959'
		,'niederrheinische-formenfabrik-gerh-janssen-sohn-gmbh-co-kg-335129'
		,'sam-baeckereitechnik-gmbh-537150'
		,'back-gastrowelt-munz-ek-1512435'
		,'boess-gmbh-1539272'
		,'djg-food-tec-gmbh-1290949'
	]
},{
	link => 'https://www.wlw.de/treffer/kaelteanlagen-fuer-baeckereien.html',
	num => 154061,
	docs => [
		'andreas-kopp-inh-reiner-schebitz-1696552'
		,'annen-verfahrenstechnik-gmbh-966775'
		,'cps-kaeltetechnik-gmbh-1465089'
		,'karl-schlegel-gmbh-965309'
		,'ke-fibertec-deutschland-gmbh-1627417'
		,'metzger-kaelte-eis-und-schneetechnik-1325338'
		,'pfeuffer-gmbh-kuehlzellenkuehlraumtuerenbrandschut-326549'
		,'walter-roller-gmbh-co-347468'
	]
},{
	link => 'https://www.wlw.de/treffer/ladeneinrichtungen-fuer-baeckereien.html',
	num => 92101,
	docs => [
		'kuehla-kuehltechnik-ladenbau-gmbh-565471'
		,'shop-exclusiv-ladenbau-und-mehr-gmbh-1214219'
		,'mobili-oekologische-einrichtungen-1499001'
		,'haerle-innenausbau-gmbh-1143607'
		,'karl-schmitt-gmbh-564955'
		,'mohn-gmbh-1026044'
		,'pst-pulverbeschichtung-und-stahlelemente-gmbh-1611338'
		,'siq-objekt-gmbh-1721012'
		,'thiem-shop-einrichtungen-gmbh-535888'
		,'warsteiner-alu-systeme-gmbh-656735'
	]
},{
	link => 'https://www.wlw.de/treffer/mehlsilos-fuer-baeckereien.html',
	num => 153397,
	docs => [
		'industriereinigung-scheid-1624245'
		,'muehlenbau-und-anlagenmontagen-gmbh-soeren-kolenda-389322'
		,'abs-silo-und-foerderanlagen-gmbh-465382'
		,'daxner-international-gmbh-298428'
		,'itt-international-technical-textiles-gmbh-1741002'
		,'rainer-wutsch-planungtechnik-1507581'
		,'zeppelin-systems-gmbh-441475'
	]
},{
	link => 'https://www.wlw.de/treffer/reinigung-von-baeckereitransportgeraeten.html',
	num => 139535,
	docs => [
		'mas-industrieservice-gmbh-553226'
		,'hinst-gmbh-562278'
		,'mas-rohrformteile-gmbh-563128'
		,'skd-system-komponenten-dienstleister-gmbh-657714'
		,'agm-dienstleistungs-gmbh-1709268'
		,'hubert-gablenz-1719827'
	]
},{
	link => 'https://www.wlw.de/treffer/reparaturen-von-baeckereimaschinen-und-baeckereianlagen.html',
	num => 95990,
	docs => [
		'otto-fischer-774922'
		,'sam-baeckereitechnik-gmbh-537150'
		,'nuermont-installations-gmbh-co-kg-300807'
		,'baktec-gmbh-1352111'
		,'ehs-elektro-anlagentechnik-ek-1711975'
		,'atr-anlagentechnik-rheinmain-ug-haftungsbeschraenkt-1567268'
		,'hbt-hanse-backtechnik-kg-1566655'
	]
},{
	link => 'https://www.wlw.de/treffer/software-fuer-baeckereien-und-konditoreien.html',
	num => 131757,
	docs => [
		'grs-software-gmbh-971883'
		,'abacus-retail-gmbh-1616388'
		,'horst-cornelissen-und-manfred-schwaller-1525773'
		,'karlheinz-huber-1607643'
		,'adupasconsult-dietmar-schlaup-1252384'
		,'winback-gmbh-1244360'
	]
}
,{
	link => 'https://www.wlw.de/treffer/sonderanlagen-fuer-die-baeckereiindustrie.html',
	num => 61622,
	docs => [
		'bmtec-gmbh-585959'
		,'srk-systemtechnik-gmbh-305422'
		,'daltec-gmbh-1019430'
		,'daxner-international-gmbh-298428'
		,'livio-bartel-gmbh-985216'
		,'sam-baeckereitechnik-gmbh-537150'
		,'beta-gmbh-1685854'
	]
}
,{
	link => 'https://www.wlw.de/treffer/staubsauger-fuer-baeckereien.html',
	num => 126577,
	docs => [
		'brueck-gmbh-co-kg-597272'
		,'dustcontrol-gmbh-285139'
		,'esta-apparatebau-gmbh-co-kg-342829'
		,'topchem-gmbh-1259163'
	]
}
,{
	link => 'https://www.wlw.de/treffer/steuerungen-fuer-baeckereioefen.html',
	num => 80332,
	docs => [
		'hemi-gmbh-1469095'
		,'a-z-automation-gmbh-588858'
	]
}
,{
	link => 'https://www.wlw.de/treffer/textilien-fuer-baeckereien.html',
	num => 161519,
	docs => [
		'itt-international-technical-textiles-gmbh-1741002'
		,'why-not-textilhandels-gmbh-1632054'
		,'luwitex-textilkonfektion-1730776'
		,'backideen-hg-gmbh-1751935'
	]
}
,{
	link => 'https://www.wlw.de/treffer/waschanlagen-fuer-baeckereien.html',
	num => 129198,
	docs => [
		'bibko-umwelt-und-reinigungstechnik-gmbh-geschaeftsbereich-platz-663302'
		,'kitzinger-maschinenbau-gmbh-339341'
		,'hospamed-reinigungstechnik-fritz-stoffels-1567197'
		,'brueel-international-gmbh-1433425'
		,'kerres-anlagensysteme-gmbh-325889'
		,'ludwig-bohrer-maschinenbau-gmbh-285749'
		,'rudolf-schlee-gmbh-276430'
	]
}
,{
	link => 'https://www.wlw.de/treffer/werbemittel-fuer-baeckereien.html',
	num => 160868,
	docs => [
		'emil-schmidt-gmbh-1105573'
		,'1st-print-promotion-werbekalender-werbeartikel-1585193'
		,'absolut-digitaldruck-ek-289172'
		,'ackermann-kunstverlag-1014338'
		,'dw-collection-1608079'
		,'igro-import-und-grosshandelsgesellschaft-mbh-650023'
	]
}
,{
	link => 'https://www.wlw.de/treffer/baecker-und-kochbekleidung.html',
	num => 67688,
	docs => [
		'bardusch-gmbh-co-kg-554628'
		,'agl-ausstattungsservice-fuer-gastronomie-und-lebensmittelhandwerk-gmbh-584387'
		,'apaya-ag-1465552'
		,'bernhard-berr-671660'
		,'fortdress-international-kg-1633650'
		,'itt-international-technical-textiles-gmbh-1741002'
		,'jentschke-bekleidungen-fuer-industrie-u-handwerk-1115200'
		,'lusini-gmbh-1626230'
		,'merk-berufskleidung-gmbh-1551855'
		,'renate-kettler-344348'
	]
}
,{
	link => 'https://www.wlw.de/treffer/baeckerbeutel.html',
	num => 102411,
	docs => [
		'brinkmann-papier-gmbh-verpacken-und-werben-591332'
		,'frey-serviceverpackungen-inh-thomas-frey-1263394'
		,'friedrich-jahncke-gmbh-co-kg-358732'
		,'heinrich-ludwig-verpackungsmittel-gmbh-468658'
		,'nocke-verpackung-lfc-nocke-gmbh-co-kg-326683'
		,'cofia-moderne-verpackungsloesungen-fuer-gastronomie-1607254'
		,'friederichsen-sohn-papier-und-folienverarbeitung-gmbh-353676'
		,'andreas-kopp-inh-reiner-schebitz-1696552'
		,'enol-folien-gmbh-1313149'
		,'paolo-sandro-ag-429015'
		,'verpackungsvertrieb-toelle-1605544'
	]
}
,{
	link => 'https://www.wlw.de/treffer/baeckerfroster.html',
	num => 61556,
	docs => [
		'ernst-grimm-grosskuechen-und-reinigungstechnik-gmbh-655255'
		,'gram-deutschland-gmbh-1034183'
		,'kaeltetechnik-rauschenbach-gmbh-964473'
		,'kaelte-klima-stueck-gmbh-319307'
		,'wolfram-ungermann-systemkaelte-gmbh-co-kg-619447'
		,'eloma-gmbh-343482'
	]
}

,{
	link => 'https://www.wlw.de/treffer/baeckerzangen.html',
	num => 134843,
	docs => [
		'emil-schmidt-gmbh-1105573'
		,'baufeld-gmbh-1568693'
		,'k-j-importexport-grosshandel-302081'
		,'katja-strueber-gaststaetten-und-konditoreibedarf-1540396'
	]
}

);

# Create a new Excel workbook
my $workbook = Excel::Writer::XLSX->new( 'res.xlsx' );
#my $format_blank = $workbook->add_format();
my $worksheet = $workbook->add_worksheet('wlw.de');
my $worksheet1 = $workbook->add_worksheet('unique');
my $row=0;

my $col=0;

$worksheet->write($row,$col++,'Company name');
$worksheet->write($row,$col++,'Website');
$worksheet->write($row,$col++,'Contact person');
$worksheet->write($row,$col++,'Phone');
$worksheet->write($row,$col++,'Fax');
$worksheet->write($row,$col++,'Mail');
$worksheet->write($row,$col++,'Street (1)');
$worksheet->write($row,$col++,'Number (1)');
$worksheet->write($row,$col++,'Postal code (1)');
$worksheet->write($row,$col++,'City (1)');
$worksheet->write($row,$col++,'Street (2)');
$worksheet->write($row,$col++,'Number (2)');
$worksheet->write($row,$col++,'Postal code (2)');
$worksheet->write($row,$col++,'City (2)');
$worksheet->write($row,$col++,'Products');
$worksheet->write($row,$col++,'Logo');
$worksheet->write($row,$col++,'Group URL');
$worksheet->write($row,$col++,'Company URL');

my $col=0;

$worksheet1->write($row,$col++,'Company name');
$worksheet1->write($row,$col++,'Website');
$worksheet1->write($row,$col++,'Contact person');
$worksheet1->write($row,$col++,'Phone');
$worksheet1->write($row,$col++,'Fax');
$worksheet1->write($row,$col++,'Mail');
$worksheet1->write($row,$col++,'Street (1)');
$worksheet1->write($row,$col++,'Number (1)');
$worksheet1->write($row,$col++,'Postal code (1)');
$worksheet1->write($row,$col++,'City (1)');
$worksheet1->write($row,$col++,'Street (2)');
$worksheet1->write($row,$col++,'Number (2)');
$worksheet1->write($row,$col++,'Postal code (2)');
$worksheet1->write($row,$col++,'City (2)');
$worksheet1->write($row,$col++,'Products');
$worksheet1->write($row,$col++,'Logo');
$worksheet1->write($row,$col++,'Company URL');

my %firms;

my $row1=0;
foreach my $group (@data) {
	
	foreach my $doc (@{$group->{docs}}) {
		$row++;
		my $col=0;
		my %res;
		
		my $dom = Mojo::DOM->new( slurp("data/$group->{num}-$doc.html") );
		
		$res{name} = $dom->at('h1')->text
			if $dom->at('h1');
		$res{website} = $dom->at('a.website')->attr('href')
			if $dom->at('a.website');
		$res{person} = $dom->at('div.icon-user')->text
			if $dom->at('div.icon-user');
		$res{phone} = $dom->at('div.icon-tel')->text
			if $dom->at('div.icon-tel');
		$res{fax} = $dom->at('div.icon-printer')->text
			if $dom->at('div.icon-printer');
		$res{mail} = $dom->at('div.icon-send a')->text
			if $dom->at('div.icon-send a');

			
		if ($dom->find('li.place p')->[0]) {
#			say "1: ".$dom->find('li.place p')->[0]->text;
			
			($res{street1},$res{house1},$res{postcode1},$res{city1})
				= ( $dom->find('li.place p')->[0]->text =~ /^(.+)\s(\S+)\s(D-\d{5})\s(.+)$/ );
#			say "\t$res{street1}|$res{house1}|$res{postcode1}"
		}
		if ($dom->find('li.place p')->[1]) {
#			say "2: ".$dom->find('li.place p')->[1]->text;
			($res{street2},$res{house2},$res{postcode2},$res{city2})
				= ( $dom->find('li.place p')->[1]->text =~ /^(.+)\s(\S+)\s(D-\d{5})\s(.+)$/ );
#			say "\t$res{street2}|$res{house2}|$res{postcode2}"
		}


my @prod;
for my $e ($dom->find('li div.title span.name')->each) {
	push @prod,$e->text ;
#  	say $e->text;
}
$res{prod} = join ', ',@prod;
		
		
		foreach (qw(name person street1 city1 street2 city2 prod)) {
			$res{$_} = decode('utf-8', $res{$_} );
		}
		
		$worksheet->write($row,$col++,$res{name});
		$worksheet->write($row,$col++,$res{website});
		$worksheet->write($row,$col++,$res{person});
		$worksheet->write($row,$col++,$res{phone});
		$worksheet->write($row,$col++,$res{fax});
		$worksheet->write($row,$col++,$res{mail});
		$worksheet->write($row,$col++,$res{street1});
		$worksheet->write($row,$col++,$res{house1});
		$worksheet->write($row,$col++,$res{postcode1});
		$worksheet->write($row,$col++,$res{city1});
		$worksheet->write($row,$col++,$res{street2});
		$worksheet->write($row,$col++,$res{house2});
		$worksheet->write($row,$col++,$res{postcode2});
		$worksheet->write($row,$col++,$res{city2});
		$worksheet->write($row,$col++,$res{prod});
		$worksheet->write($row,$col++,$dom->at('div.logo')->to_string eq '<div class="logo"></div>' ? 'NO' : 'YES');
		$worksheet->write($row,$col++,$group->{link});
		$worksheet->write($row,$col++,"https://www.wlw.de/p/$doc?heading_id=$group->{num}");
	
	
		$firms{$res{name}}++;
		
#		say $firms{$res{name}}.'-'.$res{name};
		
		if ($firms{$res{name}} eq '1') {
		$col=0;
		$row1++;
			$worksheet1->write($row1,$col++,$res{name});
			$worksheet1->write($row1,$col++,$res{website});
			$worksheet1->write($row1,$col++,$res{person});
			$worksheet1->write($row1,$col++,$res{phone});
			$worksheet1->write($row1,$col++,$res{fax});
			$worksheet1->write($row1,$col++,$res{mail});
			$worksheet1->write($row1,$col++,$res{street1});
			$worksheet1->write($row1,$col++,$res{house1});
			$worksheet1->write($row1,$col++,$res{postcode1});
			$worksheet1->write($row1,$col++,$res{city1});
			$worksheet1->write($row1,$col++,$res{street2});
			$worksheet1->write($row1,$col++,$res{house2});
			$worksheet1->write($row1,$col++,$res{postcode2});
			$worksheet1->write($row1,$col++,$res{city2});
			$worksheet1->write($row1,$col++,$res{prod});
			$worksheet1->write($row1,$col++,$dom->at('div.logo')->to_string eq '<div class="logo"></div>' ? 'NO' : 'YES');
			$worksheet1->write($row1,$col++,"https://www.wlw.de/p/$doc?heading_id=$group->{num}");
	
		}


	}
	
	
}

$workbook->close();

