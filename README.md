here is another crawl job coming up, if you are interested and available.
It's simpler than the ones before, but a very similar type of job. 

For all of those search result pages below we would like you to ask to pull values from each of the search result's profile pages. 


This is an example for one of those profile pages when clicking on a company on one of those search results pages above
https://www.wlw.de/p/anlagenkonstruktion-nord-gmbh-1674374?heading_id=61548

From this example page we would need:
- Anlagenkonstruktion Nord GmbH (COMPANY NAME)
- www.akn-gmbh.de (URL)
- Herr Sönningsen (CONTACT PERSON)
- Bahnhofstrasse 144 (STREET and NUMBER in separated columns)
- D-25712 Burg (POSTAL CODE)
- Everything in each of the grey boxes below "Produkte & Dienstleistungen" (=Products & Services) „Ausstechmaschinen für Bäckereien", "Backanlagen für Bäckereien", "Bäckereimaschinen"

Also please add one more column for each of the companies:with a YES or a NO, if the company has a logo or not on the search results page. 
That tells us, if these companies have paid to be present on this directory.
