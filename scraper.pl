#!/usr/bin/perl

use strict;
use Mojo::DOM;
use Mojo::Util qw(slurp trim);

use feature ('say');

opendir(my $dh, "data") || die;

while (my $file = readdir $dh) {
	next if $file =~ /^\./;
	say $file;
}
closedir $dh;