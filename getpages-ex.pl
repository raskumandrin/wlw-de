#!/usr/bin/perl

use strict;
use Mojo::DOM;
use Mojo::UserAgent;
use Mojo::Util qw(dumper spurt);
use Mojo::UserAgent::CookieJar;
use feature 'say';

my $ua = Mojo::UserAgent->new;
$ua = $ua->cookie_jar( Mojo::UserAgent::CookieJar->new );

$ua->on(start => sub {
  my ($ua, $tx) = @_;
  $tx->req->headers->header('Referer' => 'https://www.wlw.de/');
  $tx->req->headers->header('User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36');
});


my %links = (
'https://www.wlw.de/treffer/baeckerhandschuhe.html' => undef,
'https://www.wlw.de/treffer/sonderanlagen-fuer-die-baeckereiindustrie.html' => undef,

);

`mkdir -p data9`;



foreach my $link (keys %links) {
	say "$link";

	Mojo::DOM->new( $ua->get($link)->res->body )->find('div.vcard a')->each(sub {
		my $a = shift;

		


			my ( $name,$num ) = ( $a->{href} =~ /\/p\/(.+)\?heading_id=(\d+)$/ );
		
			if ( $name and $num ) {
				say "\t$a->{href}";
	#			say $a->{href}."\n\t$name\n\t$num";
				$links{$link} = $num;
			
				$ua->get( $a->{href} )->res->content->asset->move_to("data9/$num-$name.html");
			};

	});
}

spurt dumper(%links) , 'link-dump.txt';

